;COPYRIGHT JUAN PABLO NUÑO ZEPEDA Y CARLOS OSMAR LARA GARZA (2020)
	ORG	0000H
OP	EQU	12H
	;THE DATA COMES AS A NORMAL HEX NUMBER THEREFOR DON'T HAVE TO BE CONVERTED'
	;INITIAL CONFIGURATION ROUTINES
	;CONFIGURE TIMERS, SERIAL PORT AND INTERRUPTION ROUTINES
	MOV	SCON, #50H	;8 BITS UART WITH RECEPTION ENABLED
	MOV	PCON, #80H	;DOUBLE THE BAULD RATE
	MOV	TMOD, #20H	;8 BITS AUTORELOAD TIMER/COUNTER
	MOV	TH1, #0F3H	;AUTORELOAD VALUE FOR 4800BPS
	SETB	TR1		;START TIMER 1

;THIS IS A MAIN LOOP
MOV P0,#00H
MAINLOOP:
	JNB	RI, $		;WAIT FOR THE 8051 TO SET THE RI FLAG
	MOV	P0, SBUF	;READ THE CHARACTER FROM THE SERIAL PORT (MUST BE CLEARED BY SOFTWARE)
	CLR	RI
	CALL LDELAY
	JMP	MAINLOOP

GETASCIIFROMHEX:	CLR	C	;GET NUMBER FROM OP AND PUT IT IN THE ACC
	MOV	B, #0AH
	MOV	A, OP
	SUBB	A, B
	JNB	A.7, NEGATIVOHEX
	MOV	A, OP		;POSITIVO
	ADD	A, #30H
	JMP	FFF
NEGATIVOHEX:	MOV	A, OP
	ADD	A, #37H
FFF:	RET

SDELAY:	MOV	R6, #250D
	MOV	R5, #250D
LABEL1:	DJNZ	R6, LABEL1
LABEL2:	DJNZ	R5, LABEL2
	RET

LDELAY:	MOV	R5, #0AEH
BUC0:	MOV	R6, #0AEH
BUC1:	MOV	R7, #005H
BUC2:	DJNZ	R7, BUC2
	DJNZ	R6, BUC1
	DJNZ	R5, BUC0
	RET
HOLA:
	DB	'bb: odinevneiB'	;14
	END
