;COPYRIGHT JUAN PABLO NUÑO ZEPEDA Y CARLOS OSMAR LARA GARZA (2020)
	ORG	0000H
OP	EQU	12H
	;THE DATA COMES AS A NORMAL HEX NUMBER THEREFOR DON'T HAVE TO BE CONVERTED'
	;INITIAL CONFIGURATION ROUTINES
	;CONFIGURE TIMERS, SERIAL PORT AND INTERRUPTION ROUTINES
	MOV	SCON, #50H	;8 BITS UART WITH RECEPTION ENABLED
	MOV	PCON, #80H	;DOUBLE THE BAULD RATE
	MOV	TMOD, #20H	;8 BITS AUTORELOAD TIMER/COUNTER
	MOV	TH1, #0F3H	;AUTORELOAD VALUE
	SETB	TR1		;START TIMER 1

MAINLOOP:
	CALL	GETKEY		;GETKEY
	CALL	SENDCHARSERIAL
	JMP	MAINLOOP



SENDCHARSERIAL:	CLR	TI	;BE SURE THE BIT IS INITIALLY CLEAR, AND CHARGE SBUF
	MOV	SBUF, OP		;SEND THE LETTER A TO THE SERIAL PORT
	JNB	TI, $		;PAUSE UNTIL THE TI BIT IS SET.
	RET


SDELAY:	MOV	R6, #250D
	MOV	R5, #250D
LABEL1:	DJNZ	R6, LABEL1
LABEL2:	DJNZ	R5, LABEL2
	RET

LDELAY:	MOV	R5, #0AEH
BUC0:	MOV	R6, #0AEH
BUC1:	MOV	R7, #005H
BUC2:	DJNZ	R7, BUC2
	DJNZ	R6, BUC1
	DJNZ	R5, BUC0
	RET

;SUB RUTINA PARA EL TECLADO (DEBEMOS LEVANTAR UNA FLAG)
GETKEY:

BANDERA:	JB	P1.0, BANDERA
	MOV	OP, P2
	RET
	END
